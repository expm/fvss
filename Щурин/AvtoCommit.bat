@echo off

set /P Comm="Write a comment: "
echo %Comm%
git add .
git commit -m "%Comm%"

git remote set-url origin https://AndreyShurin13:fksBSyzmLp6qp5h3UfmG@bitbucket.org/expm/fvss.git
set /P Tg="Tag: "
echo %Tg%

set /P Tgd="Tag description: "
echo %Tgd%

git tag -a %Tg% -m "%Tgd%"

git fetch -v --progress "origin"
git merge master

git pull --progress "origin"
git push --progress "origin" master:master --tags

pause>nul