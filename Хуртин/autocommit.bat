@echo off

set /P Commit_description="Commit text: "
echo %Commit_description%
git add .
git commit -m "%Commit_description%"

set /P Tg="Tag: "
echo %Tg%

set /P Tgd="Tag description: "
echo %Tgd%

git tag -a %Tg% -m "%Tgd%"

git fetch -v --progress "origin"
git merge master
git pull --progress "origin"
git push --progress "origin" master:master --tags

pause>nul